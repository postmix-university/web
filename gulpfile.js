var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Gentelella vendors path : vendor/bower_components/gentelella/vendors

elixir(function(mix) {

  /********************/
  /* Copy Stylesheets */
  /********************/

  // Bootstrap
  mix.copy(
      'vendor/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css',
      'public/admin/css/bootstrap.min.css');

  // Font awesome
  mix.copy(
      'vendor/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css',
      'public/admin/css/font-awesome.min.css');

  // Gentelella
  mix.sass('admin/app.scss', 'public/admin/css')
      .styles([
        'public/admin/css/app.css',
        'vendor/bower_components/gentelella/build/css/custom.min.css',
      ], 'public/admin/css/gentelella.min.css', './');

  // Styles of guest view
  mix.styles([
        'resources/assets/sass/front/**/*.css',
      ],
      'public/guest/css/app.css', './');

  /****************/
  /* Copy Scripts */
  /****************/

  mix.copy(
      'vendor/bower_components/gentelella/vendors/jquery/dist/jquery.min.js',
      'public/admin/js/vendor/jquery.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js',
      'public/admin/js/vendor/bootstrap.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/fastclick/lib/fastclick.js',
      'public/admin/js/vendor/fastclick.js');
  mix.copy('vendor/bower_components/gentelella/vendors/nprogress/nprogress.js',
      'public/admin/js/vendor/nprogress.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/Chart.js/dist/Chart.min.js',
      'public/admin/js/vendor/Chart.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/gauge.js/dist/gauge.min.js',
      'public/admin/js/vendor/gauge.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js',
      'public/admin/js/vendor/bootstrap-progressbar.min.js');
  mix.copy('vendor/bower_components/gentelella/vendors/iCheck/icheck.min.js',
      'public/admin/js/vendor/icheck.min.js');
  mix.copy('vendor/bower_components/gentelella/vendors/skycons/skycons.js',
      'public/admin/js/vendor/skycons.js');
  mix.copy('vendor/bower_components/gentelella/vendors/Flot/jquery.flot.js',
      'public/admin/js/vendor/jquery.flot.js');
  mix.copy('vendor/bower_components/gentelella/vendors/Flot/jquery.flot.pie.js',
      'public/admin/js/vendor/jquery.flot.pie.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/Flot/jquery.flot.time.js',
      'public/admin/js/vendor/jquery.flot.time.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/Flot/jquery.flot.stack.js',
      'public/admin/js/vendor/jquery.flot.stack.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/Flot/jquery.flot.resize.js',
      'public/admin/js/vendor/jquery.flot.resize.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/flot.orderbars/js/jquery.flot.orderBars.js',
      'public/admin/js/vendor/jquery.flot.orderBars.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/flot-spline/js/jquery.flot.spline.min.js',
      'public/admin/js/vendor/jquery.flot.spline.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/flot.curvedlines/curvedLines.js',
      'public/admin/js/vendor/curvedLines.js');
  mix.copy('vendor/bower_components/gentelella/vendors/DateJS/build/date.js',
      'public/admin/js/vendor/date.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/jqvmap/dist/jquery.vmap.js',
      'public/admin/js/vendor/jquery.vmap.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/jqvmap/dist/maps/jquery.vmap.world.js',
      'public/admin/js/vendor/jquery.vmap.world.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js',
      'public/admin/js/vendor/jquery.vmap.sampledata.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/moment/min/moment.min.js',
      'public/admin/js/vendor/moment.min.js');
  mix.copy(
      'vendor/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js',
      'public/admin/js/vendor/daterangepicker.js');

  // Gentelella
  mix.copy('vendor/bower_components/gentelella/build/js/custom.min.js',
      'public/admin/js/gentelella.min.js');

  /**************/
  /* Copy Fonts */
  /**************/

  // Bootstrap
  mix.copy('vendor/bower_components/gentelella/vendors/bootstrap/fonts/',
      'public/admin/fonts');

  // Font awesome
  mix.copy('vendor/bower_components/gentelella/vendors/font-awesome/fonts/',
      'public/admin/fonts');

  // Font awesome
  mix.copy('resources/assets/img/guest/', 'public/guest/img/');
});
