<?php

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');

/**
 * Admin panel
 */
Route::group([
    'middleware' => 'auth',
    'prefix' => 'admin',
    'as' => 'admin.',
    'namespace' => 'Admin',
], function () {
    Route::get('/dashboard', [
        'as' => 'index',
        'uses' => 'DashboardController@index',
    ])->middleware('can:reports-dashboard');

    Route::resource('category', 'CategoriesController', [
        'except' => ['show'],
    ]);

    Route::resource('sale', 'SalesController', [
        'only' => 'index',
    ])->middleware('can:management-sales');

    Route::resource('product', 'ProductsController', [
        'except' => ['show'],
    ]);

    Route::resource('role', 'RolesController', [
        'except' => ['show'],
    ])->middleware('can:management-users');

    Route::resource('user', 'UsersController', [
        'except' => ['show'],
    ])->middleware('can:management-users');
});

/**
 * Guest views
 */
Route::group([
    'as' => 'guest.',
], function () {
    Route::get('/', [
        'as' => 'index',
        'uses' => 'IndexController@index',
    ]);

    Route::group([
        'as' => 'product.',
        'prefix' => 'products',
    ], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'ShopController@index',
        ]);

        Route::get('/{product}', [
            'as' => 'show',
            'uses' => 'ShopController@show',
        ]);
    });

    Route::group([
        'as' => 'cart.',
        'prefix' => 'cart',
    ], function () {
        Route::get('/', [
            'as' => 'index',
            'uses' => 'CartController@index',
        ]);

        Route::get('{product}/delete', [
            'as' => 'delete',
            'uses' => 'CartController@destroy',
        ]);

        Route::post('add', [
            'as' => 'add',
            'uses' => 'CartController@add',
        ]);

        Route::post('buy', [
            'as' => 'buy',
            'uses' => 'CartController@buy',
        ]);
    });
});
