@extends('layouts.admin')

@section('title', 'Creating Product')

@section('main_container')

    <div class="mt-2">
        @include('includes.flash')
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Creating Product</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <br/>
                    <form class="form-horizontal form-label-left"
                          method="post"
                          enctype="multipart/form-data"
                          action="{{ route('admin.product.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="category_id">Category <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <select id="category_id" name="category_id"
                                        class="form-control col-md-7 col-xs-12"
                                        required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="title">Title <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="text" id="title" name="title" maxlength="255"
                                       class="form-control col-md-7 col-xs-12"
                                       value="{{ old('title') }}"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="description">Description <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <textarea id="description"
                                          name="description"
                                          class="form-control col-md-7 col-xs-12"
                                          required>{{ old('description') }}</textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="thumbnail">Thumbnail
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="file" name="thumbnail" accept="image/*">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="amount">Amount <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" min="0" step="1" id="amount" name="amount"
                                       class="form-control col-md-7 col-xs-12"
                                       value="{{ old('amount') }}"
                                       required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3 col-sm-3 col-xs-12"
                                   for="price">Price <span
                                        class="required">*</span>
                            </label>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <input type="number" min="0.01" step="0.01" id="price" name="price"
                                       class="form-control col-md-7 col-xs-12"
                                       value="{{ old('price') }}"
                                       required>
                            </div>
                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                                <a href="{{ route('admin.product.index') }}" class="btn btn-primary"
                                   type="button">Cancel</a>
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
