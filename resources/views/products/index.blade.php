@extends('layouts.admin')

@section('title', 'Products')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <div class="mt-2">
        @include('includes.flash')
    </div>

    <!-- page content -->
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Products</h2>
                <ul class="nav navbar-right">
                    <li><a href="{{ route('admin.product.create') }}" class="btn btn-success"><i
                                    class="fa fa-plus"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="text-center">
                    {{ $products->render() }}
                </div>
                <table class="products table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Title</th>
                        <th>Category</th>
                        <th>Description</th>
                        <th>Thumbnail</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th width="120">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($products as $product)
                        <tr>
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->title }}</td>
                            <td>{{ $product->category->title }}</td>
                            <td>{{ str_limit($product->description) }}</td>
                            <td>
                                <img class="product-image"
                                     src="{{ Storage::url($product->thumbnail) }}"
                                     alt="{{ $product->title }}">
                            </td>
                            <td>{{ $product->formatted_amount }}</td>
                            <td>$ {{ $product->formatted_price }}</td>
                            <td>
                                <a href="{{ route('admin.product.edit', $product) }}"
                                   class="btn btn-warning"><i
                                            class="fa fa-pencil"></i></a>
                                <form method="post"
                                      action="{{ route('admin.product.destroy', $product) }}"
                                      class="d-inline">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="delete">
                                    <button type="submit" class="btn btn-danger">
                                        <i class="fa fa-trash"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr class="text-center">
                            <td colspan="8">
                                <h3>Data is not found</h3>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $products->render() }}
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection