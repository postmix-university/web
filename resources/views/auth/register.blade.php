<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentellela Alela! | </title>

    <!-- Bootstrap -->
    <link href="{{ asset("admin/css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("admin/css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("admin/css/gentelella.min.css") }}" rel="stylesheet">

</head>

<body class="login">
<div class="login_wrapper">
    <div class="animate form login_form">
        <section class="login_content">
            {!! BootForm::open(['url' => url('/register'), 'method' => 'post']) !!}

            <h1>Create Account</h1>

            {!! BootForm::text('first_name', 'First Name', old('first_name'), ['placeholder' => 'First Name']) !!}

            {!! BootForm::text('last_name', 'Last Name', old('last_name'), ['placeholder' => 'Last Name']) !!}

            {!! BootForm::email('email', 'Email', old('email'), ['placeholder' => 'Email']) !!}

            {!! BootForm::password('password', 'Password', ['placeholder' => 'Password']) !!}

            {!! BootForm::password('password_confirmation', 'Password confirmation', ['placeholder' => 'Confirmation']) !!}

            {!! BootForm::submit('Register', ['class' => 'btn btn-default']) !!}

            <div class="clearfix"></div>

            <div class="separator">
                <p class="change_link">Already a member ?
                    <a href="{{ url('/login') }}" class="to_register"> Log in </a>
                </p>

                <div class="clearfix"></div>
                <br/>

                <div>
                    <h1><i class="fa fa-desktop"></i> Computer Shop!</h1>
                    <p>©{{ date('Y') }} All Rights Reserved.</p>
                </div>
            </div>
            {!! BootForm::close() !!}
        </section>
    </div>
</div>
</body>
</html>