@extends('layouts.admin')

@section('title', 'Sales')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <!-- page content -->
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Sales List
                    <small>Transactions initiated by clients</small>
                </h2>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="text-center">
                    {{ $sales->render() }}
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>Product Title</th>
                        <th>Transaction Id</th>
                        <th>Transaction Datetime</th>
                        <th>Amount</th>
                        <th>Price</th>
                        <th>Profit</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $profits = []; ?>
                    @foreach($sales as $sale)
                        <?php
                        $profit = $sale->amount * $sale->product->price;
                        $profits[] = $profit;
                        ?>
                        <tr>
                            <th scope="row">{{ $sale->id }}</th>
                            <td><a href="#">{{ $sale->product->title }}</a></td>
                            <td>{{ $sale->transaction_id }}</td>
                            <td>{{ $sale->trx_datetime->format('m/d/Y H:i:s') }}</td>
                            <td>{{ $sale->formatted_amount }}</td>
                            <td>$ {{ $sale->product->formatted_price }}</td>
                            <td>$ {{ $profit }}</td>
                        </tr>
                    @endforeach
                    <tr>
                        <td colspan="6"></td>
                        <td><b>$ {{ number_format(array_sum($profits), 2) }}</b></td>
                    </tr>
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $sales->render() }}
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection