@extends('layouts.guest')

@section('content')
    <div class="slider-area">
        <div class="zigzag-bottom"></div>
        <div id="slide-list" class="carousel carousel-fade slide" data-ride="carousel">

            <div class="slide-bulletz">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ol class="carousel-indicators slide-indicators">
                                <li data-target="#slide-list" data-slide-to="0" class="active"></li>
                                <li data-target="#slide-list" data-slide-to="1"></li>
                                <li data-target="#slide-list" data-slide-to="2"></li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>

            <div class="carousel-inner" role="listbox">
                <div class="item active">
                    <div class="single-slide">
                        <div class="slide-bg slide-one"></div>
                        <div class="slide-text-wrapper">
                            <div class="slide-text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <div class="slide-content">
                                                <h2>We are awesome</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Consequuntur, dolorem,
                                                    excepturi. Dolore aliquam quibusdam ut quae iure
                                                    vero exercitationem ratione!</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Modi ab molestiae minus
                                                    reiciendis! Pariatur ab rerum, sapiente ex
                                                    nostrum laudantium.</p>
                                                <a href="" class="readmore">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="single-slide">
                        <div class="slide-bg slide-two"></div>
                        <div class="slide-text-wrapper">
                            <div class="slide-text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <div class="slide-content">
                                                <h2>We are great</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Saepe aspernatur, dolorum
                                                    harum molestias tempora deserunt voluptas
                                                    possimus quos eveniet, vitae voluptatem
                                                    accusantium atque deleniti inventore. Enim quam
                                                    placeat expedita! Quibusdam!</p>
                                                <a href="" class="readmore">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="single-slide">
                        <div class="slide-bg slide-three"></div>
                        <div class="slide-text-wrapper">
                            <div class="slide-text">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-6 col-md-offset-6">
                                            <div class="slide-content">
                                                <h2>We are superb</h2>
                                                <p>Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Dolores, eius?</p>
                                                <p>Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit. Deleniti voluptates
                                                    necessitatibus dicta recusandae quae amet nobis
                                                    sapiente explicabo voluptatibus rerum nihil quas
                                                    saepe, tempore error odio quam obcaecati
                                                    suscipit sequi.</p>
                                                <a href="" class="readmore">Learn more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div> <!-- End slider area -->

    <div class="promo-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-refresh"></i>
                        <p>30 Days return</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-truck"></i>
                        <p>Free shipping</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-lock"></i>
                        <p>Secure payments</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="single-promo">
                        <i class="fa fa-gift"></i>
                        <p>New products</p>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End promo area -->

    <div class="maincontent-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="latest-product">
                        <h2 class="section-title">Latest Products</h2>
                        <div class="product-carousel">
                            @foreach($lastProducts as $product)
                                <div class="single-product">
                                    <div class="product-f-image">
                                        <img src="{{ Storage::url($product->thumbnail) }}" alt="">
                                        <div class="product-hover">
                                            <a href="#" class="add-to-cart-link"><i
                                                        class="fa fa-shopping-cart"></i> Add to cart</a>
                                            <a href="{{ route('guest.product.show',$product) }}"
                                               class="view-details-link"><i
                                                        class="fa fa-link"></i> See details</a>
                                        </div>
                                    </div>

                                    <h2>
                                        <a href="{{ route('guest.product.show', $product) }}">{{ $product->title }}</a>
                                    </h2>

                                    <div class="product-carousel-price">
                                        <ins>
                                            $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                        <del>$ {{ $product->formatted_price }}</del>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End main content area -->

    <div class="brands-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="brand-wrapper">
                        <h2 class="section-title">Brands</h2>
                        <div class="brand-list">
                            <img src="{{ asset('guest/img/services_logo__1.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__2.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__3.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__4.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__1.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__2.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__3.jpg') }}" alt="">
                            <img src="{{ asset('guest/img/services_logo__4.jpg') }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End brands area -->

    <div class="product-widget-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Top Sales</h2>
                        <a href="{{ route('guest.product.index') }}" class="wid-view-more">View
                            All</a>
                        @foreach($topSales as $sale)
                            <div class="single-wid-product">
                                <a href="{{ route('guest.product.show', $sale->product) }}"><img
                                            src="{{ Storage::url($sale->product->thumbnail) }}"
                                            alt="{{ $sale->title }}"
                                            class="product-thumb"></a>
                                <h2>
                                    <a href="{{ route('guest.product.show', $sale->product) }}">{{ $sale->product->title }}</a>
                                </h2>
                                <div class="product-wid-rating">
                                    @for($i = 0; $i < $sale->rating; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                                <div class="product-wid-price">
                                    <ins>
                                        $ {{ number_format(round($sale->product->price - $sale->product->price * 0.2), 2) }}</ins>
                                    <del>$ {{ $sale->product->formatted_price }}</del>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Recently Viewed</h2>
                        <a href="{{ route('guest.product.index') }}" class="wid-view-more">View
                            All</a>
                        @foreach($recentlyViewed as $product)
                            <div class="single-wid-product">
                                <a href="{{ route('guest.product.show', $product) }}"><img
                                            src="{{ Storage::url($product->thumbnail) }}"
                                            alt="{{ $product->title }}"
                                            class="product-thumb"></a>
                                <h2>
                                    <a href="{{ route('guest.product.show', $product) }}">{{ $product->title }}</a>
                                </h2>
                                <div class="product-wid-rating">
                                    @for($i = 0; $i < $product->rating; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                                <div class="product-wid-price">
                                    <ins>
                                        $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                    <del>$ {{ $product->formatted_price }}</del>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-product-widget">
                        <h2 class="product-wid-title">Top New</h2>
                        <a href="{{ route('guest.product.index') }}" class="wid-view-more">View
                            All</a>
                        @foreach($newestProducts as $product)
                            <div class="single-wid-product">
                                <a href="{{ route('guest.product.show', $product) }}"><img
                                            src="{{ Storage::url($product->thumbnail) }}"
                                            alt="{{ $product->title }}"
                                            class="product-thumb"></a>
                                <h2>
                                    <a href="{{ route('guest.product.show', $product) }}">{{ $product->title }}</a>
                                </h2>
                                <div class="product-wid-rating">
                                    @for($i = 0; $i < $product->rating; $i++)
                                        <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                                <div class="product-wid-price">
                                    <ins>
                                        $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                    <del>$ {{ $product->formatted_price }}</del>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- End product widget area -->

@stop
