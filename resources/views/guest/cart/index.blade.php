@extends('layouts.guest')

@section('title', 'Cart')

@section('content')

    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shopping Cart</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        @foreach($products as $product)
                            <div class="thubmnail-recent">
                                <img src="{{ Storage::url($product->thumbnail) }}"
                                     class="recent-thumb" alt="{{ $product->title }}">
                                <h2><a href="{{ route('guest.product.show', $product) }}"
                                       target="_blank">{{ $product->title }}</a></h2>
                                <div class="product-sidebar-price">
                                    @if (rand(0, 100) <= 20)
                                        <ins>
                                            $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                        <del>$ {{ $product->formatted_price }}</del>
                                    @else
                                        <ins>$ {{ $product->formatted_price }}</ins>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Recent Posts</h2>
                        <ul>
                            @foreach($products as $product)
                                <li>
                                    <a href="{{ route('guest.product.show', $product) }}"
                                       target="_blank">{{ $product->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="woocommerce">
                            <form method="post" action="{{ route('guest.cart.buy') }}">
                                @csrf
                                <table cellspacing="0" class="shop_table cart">
                                    <thead>
                                    <tr>
                                        <th class="product-remove">&nbsp;</th>
                                        <th class="product-thumbnail">Thumbnail</th>
                                        <th class="product-name">Product</th>
                                        <th class="product-price">Price</th>
                                        <th class="product-quantity">Quantity</th>
                                        <th class="product-subtotal">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @forelse($cart as $product)
                                        <tr class="cart_item">
                                            <td class="product-remove">
                                                <a title="Remove this item" class="remove"
                                                   href="{{ route('guest.cart.delete', $product) }}">×</a>
                                            </td>

                                            <td class="product-thumbnail">
                                                <a href="{{ route('guest.product.show', $product) }}"
                                                   target="_blank">
                                                    <img width="145"
                                                         height="145"
                                                         alt="poster_1_up"
                                                         class="shop_thumbnail"
                                                         src="{{ Storage::url($product->thumbnail) }}"></a>
                                            </td>

                                            <td class="product-name">
                                                <a href="{{ route('guest.product.show', $product) }}"
                                                   target="_blank">{{ $product->title }}</a>
                                            </td>

                                            <td class="product-price">
                                                <span class="amount"
                                                      data-price="{{ $product->price }}">$ {{ $product->formatted_price }}</span>
                                            </td>

                                            <td class="product-quantity">
                                                <div class="quantity buttons_added">
                                                    <input type="button" class="minus" value="-">
                                                    <input type="number" size="4"
                                                           class="input-text qty text" title="Qty"
                                                           value="{{ $product->amount }}" min="1"
                                                           step="1">
                                                    <input type="button" class="plus" value="+">
                                                </div>
                                            </td>

                                            <td class="product-subtotal">
                                                <span class="amount">$ {{ number_format($product->price * $product->amount, 2) }}</span>
                                            </td>
                                        </tr>
                                    @empty
                                        <tr>
                                            <td colspan="6">
                                                <span>Here is no any products</span>
                                            </td>
                                        </tr>
                                    @endforelse
                                    @if ($cart->count() > 0)
                                        <tr>
                                            <td class="actions" colspan="6">
                                                <input type="submit" value="Buy"
                                                       name="proceed"
                                                       class="checkout-button button alt wc-forward">
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </form>

                            <div class="cart-collaterals">


                                <div class="cross-sells">
                                    <h2>You may be interested in...</h2>
                                    <ul class="products">
                                        @foreach($relatedProducts as $product)
                                            <li class="product">
                                                <a href="{{ route('guest.product.show', $product) }}"
                                                   target="_blank">
                                                    <img width="325" height="325" alt="T_4_front"
                                                         class="attachment-shop_catalog wp-post-image"
                                                         src="{{ Storage::url($product->thumbnail) }}">
                                                    <h3>{{ $product->title }}</h3>
                                                    <span class="price"><span
                                                                class="amount">$ {{ $product->formatted_price }}</span></span>
                                                </a>

                                                <a class="add_to_cart_button" data-quantity="1"
                                                   data-product_sku="" data-product_id="22"
                                                   rel="nofollow"
                                                   target="_blank"
                                                   href="{{ route('guest.product.show', $product) }}">Select
                                                    options</a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>


                                <div class="cart_totals">
                                    <h2>Cart Totals</h2>

                                    <table cellspacing="0">
                                        <tbody>
                                        <tr class="cart-subtotal">
                                            <th>Cart Subtotal</th>
                                            <td>
                                                <span class="amount">$ {{ number_format($cart->pluck('global_price')->sum(), 2) }}</span>
                                            </td>
                                        </tr>

                                        <tr class="shipping">
                                            <th>Shipping and Handling</th>
                                            <td>Free Shipping</td>
                                        </tr>

                                        <tr class="order-total">
                                            <th>Order Total</th>
                                            <td><strong><span
                                                            class="amount">$ {{ number_format($cart->pluck('global_price')->sum(), 2) }}</span></strong>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
