@extends('layouts.guest')

@section('title', 'All products')

@section('content')
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>Shop</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row text-center">
                {{ $products->render() }}
            </div>

            <div class="row">
                @foreach($products->chunk(4) as $productsUnit)
                    <div class="row">
                        @foreach($productsUnit as $product)
                            <div class="col-md-3 col-sm-6">
                                <div class="single-shop-product">
                                    <div class="product-upper">
                                        <img src="{{ Storage::url($product->thumbnail) }}" alt="">
                                    </div>
                                    <h2>
                                        <a href="{{ route('guest.product.show', $product) }}">{{ $product->title }}</a>
                                    </h2>
                                    <div class="product-carousel-price">
                                        @if (rand(0, 100) <= 20)
                                            <ins>
                                                $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                            <del>$ {{ $product->formatted_price }}</del>
                                        @else
                                            <ins>$ {{ $product->formatted_price }}</ins>
                                        @endif
                                    </div>

                                    <div class="product-option-shop">
                                        <a class="add_to_cart_button"
                                           data-quantity="1"
                                           data-product_sku=""
                                           data-product_id="70" rel="nofollow"
                                           href="#">Add to cart</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                @endforeach
            </div>

            <div class="row text-center">
                {{ $products->render() }}
            </div>
        </div>
    </div>
@stop
