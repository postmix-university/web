@extends('layouts.guest')

@section('title', $product->title)

@section('content')
    <div class="product-big-title-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="product-bit-title text-center">
                        <h2>{{ $product->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="single-product-area">
        <div class="zigzag-bottom"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Products</h2>
                        @foreach($products as $productUnit)
                            <div class="thubmnail-recent">
                                <img src="{{ Storage::url($productUnit->thumbnail) }}"
                                     class="recent-thumb" alt="{{ $productUnit->title }}">
                                <h2>
                                    <a href="{{ route('guest.product.show', $productUnit) }}">{{ $productUnit->title }}</a>
                                </h2>
                                <div class="product-sidebar-price">
                                    @if (rand(0, 100) <= 20)
                                        <ins>
                                            $ {{ number_format(round($productUnit->price - $productUnit->price * 0.2), 2) }}</ins>
                                        <del>$ {{ $productUnit->formatted_price }}</del>
                                    @else
                                        <ins>$ {{ $productUnit->formatted_price }}</ins>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    </div>

                    <div class="single-sidebar">
                        <h2 class="sidebar-title">Recent Posts</h2>
                        <ul>
                            @foreach($products as $productUnit)
                                <li>
                                    <a href="{{ route('guest.product.show', $productUnit) }}">{{ $productUnit->title }}</a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

                <div class="col-md-8">
                    <div class="product-content-right">
                        <div class="product-breadcroumb">
                            <a href="{{ route('guest.index') }}">Home</a>
                            <a href="#">{{ $product->category->title }}</a>
                            <a href="#">{{ $product->title }}</a>
                        </div>

                        <div class="row">
                            <div class="col-sm-6">
                                <div class="product-images">
                                    <div class="product-main-img">
                                        <img src="{{ Storage::url($product->thumbnail) }}" alt="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">
                                <div class="product-inner">
                                    <h2 class="product-name">{{ $product->title }}</h2>
                                    <div class="product-inner-price">
                                        @if (rand(0, 100) <= 20)
                                            <ins>
                                                $ {{ number_format(round($product->price - $product->price * 0.2), 2) }}</ins>
                                            <del>$ {{ $product->formatted_price }}</del>
                                        @else
                                            <ins>$ {{ $product->formatted_price }}</ins>
                                        @endif
                                    </div>

                                    <form action="{{ route('guest.cart.add') }}" method="post"
                                          class="cart">
                                        @csrf
                                        <input type="hidden" name="product_id"
                                               value="{{ $product->id }}">
                                        <div class="quantity">
                                            <input type="number" size="4"
                                                   class="input-text qty text" title="Qty" value="1"
                                                   name="quantity" min="1" step="1">
                                        </div>
                                        <button class="add_to_cart_button" type="submit">Add to
                                            cart
                                        </button>
                                    </form>

                                    <div class="product-inner-category">
                                        <p>Category: <a href="#">{{ $product->category->title }}</a>.
                                            Tags: <a href="#">awesome</a>, <a href="#">best</a>, <a
                                                    href="#">sale</a>, <a href="#">shoes</a>. </p>
                                    </div>

                                    <div role="tabpanel">
                                        <ul class="product-tab" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#home" aria-controls="home" role="tab"
                                                   data-toggle="tab">Description</a>
                                            </li>
                                            <li role="presentation"><a href="#profile"
                                                                       aria-controls="profile"
                                                                       role="tab" data-toggle="tab">Reviews</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane fade in active"
                                                 id="home">
                                                <h2>Product Description</h2>
                                                <p>{{ $product->description }}</p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane fade" id="profile">
                                                <h2>Reviews</h2>
                                                <div class="submit-review">
                                                    <p><label for="name">Name</label> <input
                                                                name="name" type="text"></p>
                                                    <p><label for="email">Email</label> <input
                                                                name="email" type="email"></p>
                                                    <div class="rating-chooser">
                                                        <p>Your rating</p>

                                                        <div class="rating-wrap-post">
                                                            @for ($i = 0; $i < $product->rating; $i++)
                                                                <i class="fa fa-star"></i>
                                                            @endfor
                                                        </div>
                                                    </div>
                                                    <p><label for="review">Your review</label>
                                                        <textarea name="review" id="" cols="30"
                                                                  rows="10"></textarea></p>
                                                    <p><input type="submit" value="Submit"></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>


                        <div class="related-products-wrapper">
                            <h2 class="related-products-title">Related Products</h2>
                            <div class="related-products-carousel">
                                @foreach($relatedProducts as $relatedProduct)
                                    <div class="single-product">
                                        <div class="product-f-image">
                                            <img src="{{ Storage::url($relatedProduct->thumbnail) }}"
                                                 alt="{{ $relatedProduct->title }}">
                                            <div class="product-hover">
                                                <a href="#" class="add-to-cart-link">
                                                    <i class="fa fa-shopping-cart"></i> Add to cart</a>
                                                <a href="{{ route('guest.product.show', $product) }}"
                                                   target="_blank"
                                                   class="view-details-link"><i
                                                            class="fa fa-link"></i> See details</a>
                                            </div>
                                        </div>

                                        <h2><a href="">{{ $relatedProduct->title }}</a></h2>

                                        <div class="product-carousel-price">
                                            @if (rand(0, 100) <= 20)
                                                <ins>
                                                    $ {{ number_format(round($relatedProduct->price - $relatedProduct->price * 0.2), 2) }}</ins>
                                                <del>$ {{ $relatedProduct->formatted_price }}</del>
                                            @else
                                                <ins>$ {{ $relatedProduct->formatted_price }}</ins>
                                            @endif
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
