@extends('layouts.admin')

@section('title', 'Users')

@push('stylesheets')
    <!-- Example -->
    <!--<link href=" <link href="{{ asset("css/myFile.min.css") }}" rel="stylesheet">" rel="stylesheet">-->
@endpush

@section('main_container')

    <div class="mt-2">
        @include('includes.flash')
    </div>

    <!-- page content -->
    <div class="col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>Users</h2>
                <ul class="nav navbar-right">
                    <li><a href="{{ route('admin.user.create') }}" class="btn btn-success"><i
                                    class="fa fa-plus"></i></a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content">
                <div class="text-center">
                    {{ $users->render() }}
                </div>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th width="120">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($users as $user)
                        <tr>
                            <th scope="row">{{ $user->id }}</th>
                            <td>{{ $user->first_name }}</td>
                            <td>{{ $user->last_name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->role->title }}</td>
                            <td>
                                <a href="{{ route('admin.user.edit', $user) }}"
                                   class="btn btn-warning"><i
                                            class="fa fa-pencil"></i></a>
                                @if($user->id !== auth()->user()->id)
                                    <form method="post"
                                          action="{{ route('admin.user.destroy', $user) }}"
                                          class="d-inline">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="_method" value="delete">
                                        <button type="submit" class="btn btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </form>
                                @endif
                            </td>
                        </tr>
                    @empty
                        <tr class="text-center">
                            <td colspan="3">
                                <h3>Data is not found</h3>
                            </td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
                <div class="text-center">
                    {{ $users->render() }}
                </div>
            </div>
        </div>
    </div>
    <!-- /page content -->
@endsection