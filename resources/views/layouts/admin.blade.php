<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title') | Computer Shop!</title>

    <!-- Bootstrap -->
    <link href="{{ asset("admin/css/bootstrap.min.css") }}" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="{{ asset("admin/css/font-awesome.min.css") }}" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="{{ asset("admin/css/gentelella.min.css") }}" rel="stylesheet">

    @stack('stylesheets')

</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">

        @include('includes/sidebar')

        @include('includes/topbar')

        <div class="right_col" role="main">
            @yield('main_container')
        </div>

        @include('includes/footer')

    </div>
</div>

<script src="{{ asset('admin/js/vendor/jquery.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/bootstrap.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/fastclick.js') }}"></script>
<script src="{{ asset('admin/js/vendor/nprogress.js') }}"></script>
<script src="{{ asset('admin/js/vendor/Chart.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/gauge.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/bootstrap-progressbar.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/icheck.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/skycons.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.pie.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.time.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.stack.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.resize.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.orderBars.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.flot.spline.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/curvedLines.js') }}"></script>
<script src="{{ asset('admin/js/vendor/date.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.vmap.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.vmap.world.js') }}"></script>
<script src="{{ asset('admin/js/vendor/jquery.vmap.sampledata.js') }}"></script>
<script src="{{ asset('admin/js/vendor/moment.min.js') }}"></script>
<script src="{{ asset('admin/js/vendor/daterangepicker.js') }}"></script>
<script src="{{ asset("admin/js/gentelella.min.js") }}"></script>

@stack('scripts')

</body>
</html>