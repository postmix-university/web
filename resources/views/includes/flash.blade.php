@foreach (['info' => 'info', 'warning' => 'warning', 'error' => 'danger', 'success' => 'success', 'message' => 'success'] as $class => $key)
    @if(Session::has($class))
        <div class="alert alert-{{ $key }} fade in alert-dismissible">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <h4>{{ Session::get($class) }}</h4>
        </div>
    @endif
@endforeach

@if (isset($errors) && count($errors) > 0)
    <div class="alert alert-danger fade in alert-dismissible">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        @foreach ($errors->all() as $error)
            <h4><i class="fa fa-exclamation-triangle"></i> {{ $error }}</h4>
        @endforeach
    </div>
@endif