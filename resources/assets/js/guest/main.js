jQuery(document).ready(function($) {

  // jQuery sticky Menu

  $('.mainmenu-area').sticky({ topSpacing: 0 });

  $('.product-carousel').owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 5,
      },
    },
  });

  $('.related-products-carousel').owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 2,
      },
      1000: {
        items: 2,
      },
      1200: {
        items: 3,
      },
    },
  });

  $('.brand-list').owlCarousel({
    loop: true,
    nav: true,
    margin: 20,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 4,
      },
    },
  });

  // Bootstrap Mobile Menu fix
  $('.navbar-nav li a').click(function() {
    $('.navbar-collapse').removeClass('in');
  });

  // jQuery Scroll effect
  $('.navbar-nav li a, .scroll-to-up').bind('click', function(event) {
    var $anchor = $(this);
    var headerH = $('.header-area').outerHeight();
    $('html, body').stop().animate({
      scrollTop: $($anchor.attr('href')).offset().top - headerH + 'px',
    }, 1200, 'easeInOutExpo');

    event.preventDefault();
  });

  // Bootstrap ScrollPSY
  $('body').scrollspy({
    target: '.navbar-collapse',
    offset: 95,
  });
});

/**
 * Controls of cart
 */
$('.quantity input[type="button"]').click(function(e) {
  if (e.currentTarget.value === '+') {
    $(e.currentTarget).siblings('.qty').val(function() {
      return parseInt($(this).val()) + 1;
    }).trigger('change');
  } else if (e.currentTarget.value === '-') {
    $(e.currentTarget).siblings('.qty').val(function() {
      var currentValue = parseInt($(this).val());
      if (currentValue === 1) {
        return 1;
      }
      return currentValue - 1;
    }).trigger('change');
  }
});

$('.quantity .qty').change(function() {
  var sum = 0;
  $('.shop_table.cart tbody tr.cart_item').each(function(index, el) {
    var $el = $(el);
    sum += parseFloat($el.find('.amount').attr('data-price')) *
        parseInt($el.find('.qty').val());
  });

  sum = sum.toFixed(2);

  $('.cart_item .product-subtotal .amount').html('$ ' + sum);

  var totals = $('.cart_totals');
  totals.find('.cart-subtotal .amount').html('$ ' + sum);
  totals.find('.order-total .amount').html('$ ' + sum);
});
