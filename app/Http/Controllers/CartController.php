<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;
use App\Repositories\SaleRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CartController extends Controller
{
    const SESSION_KEY = 'cart';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productRepository = app(ProductRepository::class);

        $cart = $productRepository->with('category')
            ->findWhereIn('id',
                session()->get(self::SESSION_KEY, collect())->pluck('id')->toArray());

        $products = $productRepository->scopeQuery(function ($query) use ($cart) {
            return $query->when($cart->count() !== 0, function ($q) use ($cart) {
                return $q->whereIn('category_id', $cart->pluck('category.id'));
            })
                ->selectRaw('*, ROUND((RAND() * (5 - 4)) + 4) as rating')
                ->inRandomOrder()
                ->take(4);
        })
            ->all();

        $sessionCart = session()->get(self::SESSION_KEY, collect());
        $cart->each(function ($element) use ($sessionCart) {
            $element->amount = (int)$sessionCart->where('id',
                $element->id)->first()->get('quantity');
            $element->global_price = $element->amount * $element->price;
        });

        return view('guest.cart.index', [
            'cart' => $cart,
            'products' => $products,
            'relatedProducts' => $products->take(2),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request)
    {
        session()->put(self::SESSION_KEY, session()->get(self::SESSION_KEY,
            collect())->push(collect([
            'id' => $request->get('product_id'),
            'quantity' => $request->get('quantity'),
        ])));
        return redirect()->route('guest.cart.index');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function buy()
    {
        $cart = session()->get(self::SESSION_KEY, collect());
        $saleRepository = app(SaleRepository::class);
        foreach ($cart as $product) {
            $saleRepository->create([
                'product_id' => $product->get('id'),
                'transaction_id' => uniqid(),
                'trx_datetime' => Carbon::now()->format('Y-m-d H:i:s'),
                'amount' => $product->get('quantity'),
            ]);
        }
        session()->remove(self::SESSION_KEY);
        return redirect()->route('guest.cart.index')->with('success',
            'The products have been bought!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $product
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $cart = session()->get(self::SESSION_KEY, collect());
        session()->put(self::SESSION_KEY, $cart->filter(function ($index, $element) use ($product) {
            dd($index, $element, $product->id);
        }));
        return redirect()->back();
    }
}
