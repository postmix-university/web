<?php

namespace App\Http\Controllers;

use App\Repositories\SaleRepository;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $todaySales = \Cache::remember('today_sales_' . date('Y-m-d'), 15, function () {
            $saleRepository = app(SaleRepository::class);
            return $saleRepository->scopeQuery(function ($query) {
                return $query->whereRaw('left(trx_datetime, 10) = ?', date('Y-m-d'));
            })
                ->all()
                ->count();
        });

        \View::share('todaySales', $todaySales);
    }
}
