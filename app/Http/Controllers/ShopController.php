<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Repositories\ProductRepository;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productRepository = app(ProductRepository::class);
        $products = $productRepository->paginate(12);

        return view('guest.products.index', [
            'products' => $products,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $product->load('category');

        $productRepository = app(ProductRepository::class);
        $products = $productRepository->scopeQuery(function ($query) use ($product) {
            return $query->where('category_id', $product->category_id)
                ->selectRaw('*, ROUND((RAND() * (5 - 4)) + 4) as rating')
                ->inRandomOrder()
                ->take(15);
        })
            ->all();

        return view('guest.products.show', [
            'product' => $product,
            'products' => $products->take(4),
            'relatedProducts' => $products,
        ]);
    }
}
