<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Sale;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class DashboardController extends Controller
{
    /**
     * Generate metrics for main header
     *
     * @param Model $model
     * @param int $daysRange
     *
     * @return array
     */
    private function makeHeaderData(Model $model, $daysRange = 7)
    {
        $count = $model->count();
        $countComeBeforePastWeek = $model->where('created_at', '<',
            Carbon::now()->addDays(-$daysRange)->format('Y-m-d'))->count();
        $countComePastWeek = $model->where('created_at', '>=',
            Carbon::now()->addDays(-$daysRange)->format('Y-m-d'))->count();

        return [
            number_format($count, 0),
            number_format($countComeBeforePastWeek === 0 ? 100 : (1 - $countComePastWeek / $countComeBeforePastWeek) * 100,
                2),
        ];
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        list($usersCount, $usersComePercent) = $this->makeHeaderData(new User());
        list($salesCount, $salesComePercent) = $this->makeHeaderData(new Sale());
        list($productsCount, $productsComePercent) = $this->makeHeaderData(new Product(), 30);
        $salesOfCurrentMonth = Sale::with('product')->where('trx_datetime', '>',
            Carbon::now()->day(1)->setTime(0, 0, 0)->format('Y-m-d H:i:s'))->get();
        $salesOfPreviousMonth = Sale::with('product')->where('trx_datetime', '<=',
            Carbon::now()->day(1)->setTime(0, 0, 0)->format('Y-m-d H:i:s'))->get();

        foreach ($salesOfCurrentMonth as $sale) {
            $sale->profit = $sale->product->price * $sale->amount;
        }
        foreach ($salesOfPreviousMonth as $sale) {
            $sale->profit = $sale->product->price * $sale->amount;
        }
        $salesOfCurrentMonth = $salesOfCurrentMonth->sum('profit');
        $salesOfPreviousMonth = $salesOfPreviousMonth->sum('profit');

        $usersOnline = \Cache::remember('users_inline', 1, function () {
            return rand(100, 1000);
        });

        $processingChargebacks = \Cache::remember('processing_chargebacks', 1, function () {
            return rand(0, 25);
        });

        return view('dashboard', [
            'usersCount' => $usersCount,
            'usersComePercent' => $usersComePercent,
            'salesSumCurrentMonth' => number_format($salesOfCurrentMonth, 2),
            'salesSumPercent' => number_format($salesOfPreviousMonth === 0 ? 100 : (1 - $salesOfCurrentMonth / $salesOfPreviousMonth) * 100,
                2),
            'salesCount' => $salesCount,
            'salesComePercent' => $salesComePercent,
            'productsCount' => $productsCount,
            'productsComePercent' => $productsComePercent,
            'usersOnline' => $usersOnline,
            'processingChargebacks' => $processingChargebacks,
        ]);
    }
}
