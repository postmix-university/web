<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\SaleRepository;

/**
 * Class SalesController.
 *
 * @package namespace App\Http\Controllers;
 */
class SalesController extends Controller
{
    /**
     * @var SaleRepository
     */
    protected $repository;

    /**
     * SalesController constructor.
     *
     * @param SaleRepository $repository
     */
    public function __construct(SaleRepository $repository)
    {
        parent::__construct();
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->repository->pushCriteria(app('Prettus\Repository\Criteria\RequestCriteria'));
        $sales = $this->repository->with('product')->paginate();

        if (request()->wantsJson()) {

            return response()->json([
                'data' => $sales,
            ]);
        }

        return view('sales.index', compact('sales'));
    }
}
