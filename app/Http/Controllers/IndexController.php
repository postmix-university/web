<?php

namespace App\Http\Controllers;

use App\Repositories\ProductRepository;
use App\Repositories\SaleRepository;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $productRepository = app(ProductRepository::class);
        $saleRepository = app(SaleRepository::class);

        $lastProducts = $productRepository->scopeQuery(function ($query) {
            return $query->orderBy('created_at', 'desc')
                ->selectRaw('*, ROUND((RAND() * (5 - 4)) + 4) as rating')
                ->take(15);
        })->all();

        $topSales = $saleRepository->with('product')
            ->scopeQuery(function ($query) {
                return $query->groupBy('product_id')
                    ->selectRaw('count(*) * amount as count, product_id, ROUND((RAND() * (5 - 4)) + 4) as rating')
                    ->orderBy('count', 'desc')
                    ->take(3);
            })
            ->all();

        $recentlyViewed = $productRepository->scopeQuery(function ($query) {
            return $query->inRandomOrder()
                ->selectRaw('*, ROUND((RAND() * (5 - 4)) + 4) as rating')
                ->take(3);
        })
            ->all();

        return view('guest.index', [
            'lastProducts' => $lastProducts,
            'topSales' => $topSales,
            'recentlyViewed' => $recentlyViewed,
            'newestProducts' => $lastProducts->take(3),
        ]);
    }
}
