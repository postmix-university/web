<?php

namespace App\Repositories;

use App\Models\Sale;
use App\Validators\SaleValidator;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class SaleRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class SaleRepositoryEloquent extends BaseRepositoryEloquent implements SaleRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Sale::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return SaleValidator::class;
    }
}
