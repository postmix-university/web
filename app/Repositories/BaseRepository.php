<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BaseRepository.
 *
 * @package namespace App\Repositories;
 */
interface BaseRepository extends RepositoryInterface
{
    /**
     * Remote all rows from DB
     *
     * @return mixed
     */
    public function deleteAll();

    /**
     * Get first model in random order
     *
     * @return Model
     */
    public function firstRandom();
}
