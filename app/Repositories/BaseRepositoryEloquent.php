<?php

namespace App\Repositories;

use App\Models\Category;
use App\Validators\CategoryValidator;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Eloquent\BaseRepository;

/**
 * Class BaseRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class BaseRepositoryEloquent extends BaseRepository implements \App\Repositories\BaseRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {

        return CategoryValidator::class;
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * Remote all rows from DB
     *
     * @return mixed
     */
    public function deleteAll()
    {
        return (new $this->model())->query()->delete();
    }

    /**
     * Get first model in random order
     *
     * @return Model
     */
    public function firstRandom()
    {
        return (new $this->model())->query()->inRandomOrder()->first();
    }
}
