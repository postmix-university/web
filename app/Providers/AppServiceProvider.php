<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['request']->server->set('HTTPS', true);
        $this->loadGates();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Load gates
     */
    private function loadGates()
    {
        /**
         * Only admins are able to see dashboard reports
         */
        \Gate::define('reports-dashboard', function (User $user) {
            return $user->role->slug === 'admin';
        });

        /**
         * Only admins are able to manage users and roles
         */
        \Gate::define('management-users', function (User $user) {
            return $user->role->slug === 'admin';
        });

        /**
         * Only admins are able to manage sales
         */
        \Gate::define('management-sales', function (User $user) {
            return $user->role->slug === 'admin';
        });

        /**
         * Only admins are able to manage extra parts of the site
         */
        \Gate::define('management-administration', function (User $user) {
            return $user->role->slug === 'admin';
        });
    }
}
