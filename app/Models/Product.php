<?php

namespace App\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Product.
 *
 * @package namespace App\Models;
 */
class Product extends Model
{
    use Sluggable;

    /**
     * Default photo
     */
    const DEFAULT_PHOTO_PATH = 'thumbnails/product_default.png';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_id',
        'title',
        'slug',
        'description',
        'thumbnail',
        'price',
        'amount',
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Binding events
     */
    public static function boot()
    {
        parent::boot();

        /**
         * Delete physical file from storage during deleting a row from DB
         */
        static::deleting(function (Product $product) {
            if ($product->thumbnail !== self::DEFAULT_PHOTO_PATH) {
                \Storage::disk('public')->delete($product->thumbnail);
            }
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Sales of current product
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales()
    {
        return $this->hasMany(Sale::class);
    }

    /**
     * Get formatted price
     *
     * @return string
     */
    public function getFormattedPriceAttribute()
    {
        return number_format($this->price, 2);
    }

    /**
     * Get formatted price
     *
     * @return string
     */
    public function getFormattedAmountAttribute()
    {
        return number_format($this->amount);
    }
}
