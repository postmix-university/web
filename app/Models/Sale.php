<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Sale.
 *
 * @package namespace App\Models;
 */
class Sale extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'transaction_id',
        'trx_datetime',
        'amount',
    ];

    /**
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'trx_datetime',
    ];

    /**
     * Product of current sales data
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }

    /**
     * Return formatted amount
     *
     * @return string
     */
    public function getFormattedAmountAttribute()
    {
        return number_format($this->amount);
    }
}
