<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categoryRepository = app()->make(\App\Repositories\CategoryRepository::class);
        $categoryRepository->deleteAll();

        factory(\App\Models\Category::class, 20)->create();
    }
}
