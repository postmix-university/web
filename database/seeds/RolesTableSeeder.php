<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleRepository = app()->make(\App\Repositories\RoleRepository::class);
        $roleRepository->deleteAll();

        $roleRepository->create([
            'title' => 'Admin',
        ]);

        $roleRepository->create([
            'title' => 'Manager',
        ]);

        factory(\App\Models\Role::class, 3)->create();
    }
}
