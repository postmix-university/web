<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()->make(\App\Repositories\ProductRepository::class)->deleteAll();

        factory(\App\Models\Product::class, 500)->create();
    }
}
