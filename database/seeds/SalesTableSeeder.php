<?php

use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()->make(\App\Repositories\SaleRepository::class)->deleteAll();

        factory(\App\Models\Sale::class, 200)->create();
    }
}
