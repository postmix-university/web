<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\User::truncate();

        $roleRepository = app()->make(\App\Repositories\RoleRepository::class);

        \App\Models\User::create([
            'first_name' => 'Admin',
            'last_name' => 'Adminov',
            'role_id' => $roleRepository->findWhere(['slug' => 'admin'])->first()->id,
            'email' => 'admin@gmail.com',
            'password' => \Illuminate\Support\Facades\Hash::make('secret'),
        ]);

        factory(\App\Models\User::class, rand(100, 300))->create();
    }
}
