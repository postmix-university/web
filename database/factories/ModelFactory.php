<?php

$factory->define(\App\Models\Role::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->word,
    ];
});


$factory->define(App\Models\User::class, function (Faker\Generator $faker) {
    $role = app()->make(\App\Repositories\RoleRepository::class)->firstRandom();

    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'role_id' => $role->id,
        'email' => $faker->safeEmail,
        'password' => \Illuminate\Support\Facades\Hash::make('secret'),
        'remember_token' => str_random(10),
        'created_at' => \Carbon\Carbon::now()->addWeek(-rand(0, 5))->format('Y-m-d H:i:s'),
    ];
});

$factory->define(\App\Models\Category::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->jobTitle,
    ];
});

$factory->define(\App\Models\Product::class, function (Faker\Generator $faker) {
    $category = app()->make(\App\Repositories\CategoryRepository::class)->firstRandom();

    return [
        'category_id' => $category->id,
        'title' => $faker->jobTitle,
        'description' => $faker->paragraph,
        'thumbnail' => 'thumbnails/product_default.png',
        'price' => $faker->numberBetween(1000, 10000) / 100,
        'amount' => $faker->numberBetween(0, 100),
        'created_at' => \Carbon\Carbon::now()->addWeek(-rand(0, 5))->format('Y-m-d H:i:s'),
    ];
});

$factory->define(\App\Models\Sale::class, function (Faker\Generator $faker) {
    $product = app()->make(\App\Repositories\ProductRepository::class)->firstRandom();

    return [
        'product_id' => $product->id,
        'transaction_id' => $faker->numberBetween(100000, 10000000),
        'amount' => $faker->numberBetween(1, $product->amount),
        'trx_datetime' => \Carbon\Carbon::now()->addWeek(-rand(0, 5))->format('Y-m-d H:i:s'),
    ];
});
